var request = require("request");
var model = require("./models");

/* for production */
var baseurl = "http://35.176.29.235:8001/";
var client_secret = "AGw0xwe8rMdHkgpbIMsD1pUtXpJZr4mMnaz64Jc6z/P+atQEWiFMHA03As7A7tz+4Q=="

/* for development */
//var baseurl = "http://localhost:5000/";
//var client_secret = "AAlkiSNvJAXsPQyv/W1dnZACB+1bRTnOyCrSi4NQpUB/TSGSlzzR8JBW0eBWQCFjAA==";

var client_id = 2;

var authHeader =  function(clb) {
    getAccessCred(function (err, data) {
        if (err) {
            authenticate();
            clb(err,null)
        } else {
            
            if (data == null) {
                authenticate();
            } else {
           var option = {
              "Authorization": "Bearer "+ data.access_token  
            }
            clb(null,option)     
            }
           
        }
    })
}
exports.post = function (endpoint, data, clb) {
    
    authHeader(function (err,opt) {
    var fullpath = baseurl + endpoint;
          console.log(fullpath)
    if (!err) {
           
            request.post(fullpath, { headers: opt, form:data }, function (err,res, data) {
                if (err) {
                    authenticate();
                    clb(err,null)
                    
                } else {
                    

                    if (res.statusCode == "400") {
                        console.log(data)
                        data = JSON.parse(data)
                        clb(data,null)
                        
                    } else {
                        //console.log("--->",res.statusCode)
                        data = JSON.parse(data)
                        clb(null,data)    
                    }
                    
                }
            })
    } else {
        authenticate();
        }
    })
   
}

exports.get = function (endpoint, clb) {
    authHeader(function (err, opt) {
        
        if (!err) {
            
            request.get(baseurl + endpoint, { headers: opt },  function (err,res,data) {
                if (err) {
                
                authenticate();
                clb(err, null);
                } else {
            
                if (res.statusCode == "400") {
                    data = JSON.parse(data)         
                    clb(data, null)    
                } else {
                    console.log(data)
                    data = JSON.parse(data)         
                    clb(null, data)
                }
                     
                            
            }
        })         
        }else {
        authenticate();
        }
    })
   
}

var getAccessCred = function (clb) {
    model.getAccessCred(function (err, data) { 
        if (err) {            
            clb(err, null)
        } else {
            //console.log(data)
            clb(null, data)
        }
    })
}

var authenticate = function () {

    var cred = {
        client_id: client_id,
        client_secret: client_secret,
        grant_type: 'client_credentials',
        
    }
   
    request.post(baseurl + "connect/token", { form: cred }, function (err, data) {
        if (err) {
            //console.log(err.body)
        } else {
             
            model.updateCredentials(JSON.parse(data.body), function (err, data) {
                if (err) { 
                    
                } else {
                    //console.log(data);
                }
            })
        }
    })
   
}
authenticate();
setInterval(authenticate,300000)